# apertis-flatpak-runtime

Reference Flatpak runtime for Apertis.

## Building Locally

This currently needs [a separate branch of
apertis-flatdeb](https://gitlab.apertis.org/pkg/apertis-flatdeb/-/tree/wip/refi64/T8169-changes),
and `FLATDEB_REPO` should be set to the path to the local clone of the branch.

The headless and HMI runtimes can be built via:

```
./flatdeb.sh v2022pre amd64 runtimes runtimes/org.apertis.headless.yaml
./flatdeb.sh v2022pre amd64 runtimes runtimes/org.apertis.hmi.yaml
```

This will build both the .Platform and .Sdk variants of both. To build only one,
pass one of `--platform` or `--sdk` before the `runtimes` argument.

For a faster local build, you can also pass `--no-collect-source-code` and
`--no-debug-symbols` to skip gathering the source code and generating the debug
extensions.

Once the runtimes are built, they will be placed in the OSTree repository
`flatdeb-builddir/ostree-repo`. From there, they can be installed via:

```
flatpak --user remote-add --no-gpg-verify apertis $PWD/flatdeb-builddir/ostree-repo
flatpak install apertis org.apertis.{headless,hmi}.{Platform,Sdk}
```

The sample apps can be built via:

```
./flatdeb.sh v2022pre amd64 --export-bundles apps apps/org.apertis.headless.hello.yaml
./flatdeb.sh v2022pre amd64 --export-bundles apps apps/org.apertis.hmi.gnome-font-viewer.yaml
```

These will result in `.flatpak` bundles being placed inside the
`flatdeb-builddir` folder.

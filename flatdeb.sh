#!/bin/bash

if [[ -n "$FLATDEB_REPO" ]]; then
  flatdeb="$FLATDEB_REPO/run.py"
else
  if ! hash flatdeb 2>/dev/null; then
    echo 'flatdeb is not installed, and $FLATDEB_REPO is not set.' >&2
    exit 1
  fi

  flatdeb='flatdeb'
fi

if [[ $# -lt 3 ]]; then
  echo "usage: $0 <apertis release> <arch> [command...]" >&2
  exit 1
fi

release="$1"
arch="$2"
shift 2

args=(
  --build-area="$PWD/flatdeb-builddir"
  --ostree-repo="$PWD/flatdeb-builddir/ostree-repo"
  --suite="$release"
  --arch="$arch"
)

for arg in "$@"; do
  if [[ "$arg" == "app" ]]; then
    args+=(
      --runtime-branch="$release"
      app
      --app-branch="$release"
    )
  else
    args+=("$arg")
  fi
done

set -x
exec "$flatdeb" "${args[@]}"

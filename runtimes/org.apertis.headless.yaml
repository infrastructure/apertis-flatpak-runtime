---
id_prefix: org.apertis.headless

post_script: |
  set -eux
  for tool in flatpak-spawn xdg-email xdg-open; do
    ln -sf /usr/libexec/flatpak-xdg-utils/$tool /usr/bin/$tool
  done

overlays:
  - headless-overlay

add_packages:
  - busybox-gzip
  - bzip2
  - dconf-gsettings-backend
  - file
  - flac
  - flatpak-xdg-utils
  - fontconfig
  - gir1.2-freedesktop
  - gir1.2-glib-2.0
  - gstreamer1.0-plugins-base
  - gstreamer1.0-plugins-good
  - gstreamer1.0-pipewire
  - gstreamer1.0-pulseaudio
  - gstreamer1.0-tools
  - libarchive-tools
  - libavahi-client3
  - libc6
  - libcap2
  - libcurl4
  - libexpat1
  - libffi8
  - libfftw3-bin
  - libgcrypt20
  - libgirepository-1.0-1
  - libglib2.0-0
  - libgmp10
  - libicu72
  - libncursesw6
  - libnss3
  - libogg0
  - libopus0
  - libpcre2-16-0
  - libpipewire-0.3-0
  - libproxy1v5
  - libpulse0
  - libsecret-1-0
  - libstdc++6
  - libsoup2.4-1
  - libtheora0
  - libtinfo6
  - libvorbis0a
  - libudev1
  - libunwind8
  - locales-all
  - openssl
  - p11-kit
  - pipewire-bin
  - pulseaudio-utils
  - seccomp

platform:
  pre_apt_script: |
    apt-get -y -q update

sdk:
  pre_apt_script: |
    set -eux
    sed -i 's/\(^deb.*\)/\1 development/g' /etc/apt/sources.list
    apt-get -y -q update

    # Installing coreutils will have apt remove rust-coreutils first, which ends
    # up nuking several tools we need to be able to function. Save copies of
    # those first, then the copies will be removed after the install process.
    cp /bin/rm /bin/tar /bin/grep /usr/bin/diff /usr/local/bin
    echo 'DPkg::Path "/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin";' \
      > /etc/apt/apt.conf.d/dpkg-path-usrlocal
    echo 'APT{ Get { Allow-Solver-Remove-Essential 1; Allow-Remove-Essential 1; } }' \
      > /etc/apt/apt.conf.d/99remove-essential;

  post_script: |
    rm \
      /usr/local/bin/rm \
      /usr/local/bin/tar \
      /usr/local/bin/grep \
      /usr/local/bin/diff \
      /etc/apt/apt.conf.d/dpkg-path-usrlocal \
      /etc/apt/apt.conf.d/99remove-essential
    ln -sf bash /usr/bin/sh

  add_packages:
    - appstream-util
    - autoconf
    - autoconf-archive
    - automake
    - autotools-dev
    - bash
    - bison
    - busybox-static
    - ccache
    - cmake
    - coreutils
    - debhelper
    - desktop-file-utils
    - dpkg-dev
    - diffutils
    - fakeroot
    - findutils
    - flex
    - gdb
    - gdbserver
    - gnome-pkg-tools
    - gobject-introspection
    - grep
    - gzip
    - itstool
    - libarchive-dev
    - libavahi-client-dev
    - libbz2-dev
    - libc6-dbg
    - libcap-dev
    - libcurl4-openssl-dev
    - libexpat1-dev
    - libffi-dev
    - libfftw3-dev
    - libflac-dev
    - libgcrypt20-dev
    - libgirepository1.0-dev
    - libglib2.0-dev
    - libgmp-dev
    - libgstreamer1.0-dev
    - libgstreamer-plugins-base1.0-dev
    - libicu-dev
    - libncurses-dev
    - libnss3-dev
    - libogg-dev
    - libopus-dev
    - libpcre2-dev
    - libpipewire-0.3-dev
    - libp11-kit-dev
    - libproxy-dev
    - libpulse-dev
    # Needed for appstream-compose to be able to handle SVG files (this contains
    # the GdkPixbuf loader for librsvg)
    - librsvg2-common
    - libseccomp-dev
    - libsecret-1-dev
    - libsoup2.4-dev
    - libsqlite3-dev
    - libssl-dev
    - libtheora-dev
    - libtiff5-dev
    - libvorbis-dev
    - libxml2-utils
    - libudev-dev
    - libunwind-dev
    - meson
    - nasm
    - ninja-build
    - perl
    - pkg-config
    - python3
    - python3-debian
    - python3-dev
    - sed
    - strace
    - tar
    - valgrind
...

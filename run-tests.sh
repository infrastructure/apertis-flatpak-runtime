#!/bin/bash

set -e

die() {
  echo "$@" >&2
  exit 1
}

ARGV=("$@")

ROOT=$(dirname "$0" | xargs realpath)

: ${OSTREE_REPO:=$ROOT/$TARGETS_REPO}
: ${FLATPAK_ARCH:=x86_64}
: ${FLATPAK_BRANCH:=v2022pre}

RUNTIME="org.apertis.headless.Platform/$FLATPAK_ARCH/$FLATPAK_BRANCH"
LOCALE="org.apertis.headless.Platform.Locale/$FLATPAK_ARCH/$FLATPAK_BRANCH"
SDK="org.apertis.headless.Sdk/$FLATPAK_ARCH/$FLATPAK_BRANCH"
APP="org.apertis.headless.hello/$FLATPAK_ARCH/$FLATPAK_BRANCH"

EXTRA_LANG=es

########## TESTING HELPERS ##########

ostree() {
  local cmd="$1"
  shift
  command ostree "$cmd" --repo="$OSTREE_REPO" "$@"
}

ostree-ls-just-files() {
  ostree ls "$@" | tr -s ' ' | cut -d' ' -f5-
}

flatpak-run-in() {
  local args=()
  while [[ "$1" == -* ]]; do
    args+=("$1")
    shift
  done

  local app="$1"
  local cmd="$2"
  shift 2
  flatpak run "${args[@]}" --command="$cmd" "$app" "$@"
}

match-any-lines() {
  local pattern="$1"
  shift

  local tmp=$(mktemp -p $PWD)

  "$@" > "$tmp"
  grep -Pq "$pattern" "$tmp" || (
    cat "$tmp"
    die "No lines matched '$pattern', full content printed above"
  )
}

match-all-lines() {
  local pattern="$1"
  shift

  local tmp1=$(mktemp -p $PWD)
  local tmp2=$(mktemp -p $PWD)

  "$@" > "$tmp1"
  grep -P "$pattern" "$tmp1" > "$tmp2"
  diff -u "$tmp1" "$tmp2" \
    || die "Not all lines match '$pattern', failing ones printed above"
}

########## TEST RUNNER ##########

setup-environment() {
  # Flatpak really wants a system bus running so malcontent can check for
  # parental controls, which isn't an option in CI. So, we can just run this
  # inside a custom "system bus" (that's actually a session bus), then
  # libmalcontent that the service is not available and believe it's disabled.
  # Unfortunately, we then end up with warnings about polkit being unavailable,
  # but the tests can then at least *run*.
  if [[ -z "$APERTIS_FLATPAK_RUNTIME_TEST_INSIDE_WRAPPER" ]]; then
    export APERTIS_FLATPAK_RUNTIME_TEST_INSIDE_WRAPPER=1
    exec dbus-run-session "$0" "${ARGV[@]}"
  fi

  export DBUS_SYSTEM_BUS_ADDRESS="$DBUS_SESSION_BUS_ADDRESS"
  unset DBUS_SESSION_BUS_ADDRESS

  # Note that we don't assign to $XDG_DATA_HOME directly, because then deleting
  # that folder might turn out rather badly.
  DATA_HOME="$(mktemp -d)"
  trap 'rm -rf -- "$DATA_HOME"' EXIT

  export XDG_DATA_HOME="$DATA_HOME"
  # Set this to silence Flatpak's warnings about its exports not being in the
  # data dirs.
  export XDG_DATA_DIRS="$XDG_DATA_HOME/flatpak/exports/share"
  export XDG_DATA_DIRS="$XDG_DATA_DIRS:/var/lib/flatpak/exports/share"

  rm -rf ~/.var/app/org.apertis.headless.hello

  flatpak --user config --set languages "en;$EXTRA_LANG"
  flatpak --user remote-add --no-gpg-verify apertis "$OSTREE_REPO"
  flatpak install -y --user apertis "$RUNTIME" "$SDK" "$APP"

  # Copy the host's ldconfig to avoid crashes on arm/64:
  # More info: https://gitlab.apertis.org/pkg/apertis-flatdeb/-/commit/b63cad94
  for rt in $RUNTIME $SDK; do
    cp /sbin/ldconfig \
      "$DATA_HOME/flatpak/runtime/$RUNTIME/active/files/sbin/ldconfig"
  done
}

run-test() {
  local test="$1"
  (
    TEST_WORKING_DIR=$(mktemp -d)
    trap 'rm -rf -- "$TEST_WORKING_DIR"' EXIT

    (cd "$TEST_WORKING_DIR"; set -ex; $test)
  )
}

run-all-tests() {
  local failed=()

  for test in $(declare -F | grep -Eo 'test-\S+$'); do
    if [[ ${#ARGV[@]} -ne 0 ]]; then
      local match=
      for arg in "${ARGV[@]}"; do
        if [[ $test == *$arg* ]]; then
          match=1
          break
        fi
      done

      [[ -n "$match" ]] || continue
    fi

    echo "********** TEST: $test **********"
    if run-test "$test"; then
      echo "********** PASS: $test **********"
    else
      echo "********** FAIL: $test **********"
      failed+=($test)
    fi
  done

  if [[ "${#failed[@]}" -ne 0 ]]; then
    echo 'List of failed tests:'
    for test in "${failed[@]}"; do
      echo "  - $test"
    done
    exit 1
  fi

  echo 'All tests passed!'
}

########## ACTUAL UNIT TESTS ##########

setup-environment

. tests/*.sh

run-all-tests

test-locales-have-no-external-symlinks() {
  # Checks 'ostree ls' for the locale to make sure all files are either:
  # - not symlinks
  # - symlinks pointing to the same directory
  match-all-lines '^/\S+( -> [^/]+)?$' \
    ostree-ls-just-files -R "runtime/$LOCALE" files > files
}

test-user-locales() {
  for region in en_US es_ES; do
    flatpak-run-in -d "$APP" python3 -c \
      "import locale; locale.setlocale(locale.LC_ALL, '${region}.UTF-8')"
  done
}

test-no-extra-locales-included() {
  match-all-lines "^((en|$EXTRA_LANG)_|C($|.)|POSIX$)" \
    flatpak-run-in "$APP" locale -a
}

test-fontconfig-dirs() {
  curl -Lo cousine.zip https://api.fontsource.org/v1/download/cousine

  rm -rf "$DATA_HOME/fonts"
  mkdir "$DATA_HOME/fonts"
  # Run bsdtar in the runtime so that we don't need unzip on the host system.
  flatpak-run-in --filesystem="$DATA_HOME/fonts" --filesystem="$PWD" "$APP" \
    bsdtar -C "$DATA_HOME/fonts" -xf cousine.zip --strip-components=1 'ttf/*'

  flatpak-run-in "$APP" fc-cache -fv
  match-any-lines '/run/host/user-fonts/cousine' \
    flatpak-run-in "$APP" fc-list
}
